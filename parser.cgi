#!/usr/bin/perl -T

use CGI qw/:standard *pre *table :html3 -private_tempfiles -oldstyle_urls/;
use CGI::Carp qw(fatalsToBrowser);
#use strict;

$CGI::POST_MAX=1024 * 500;  # max 500K posts

use POSIX qw(strftime);
use IPC::Open2;
use IO::Socket;

$ENV{'PATH'} = '/bin:/usr/local/bin';
$ENV{'DOTFONTPATH'} = '/usr/lib/openoffice/share/fonts/truetype';

my $query = new CGI;
my $pattern = qr/(\w+)\s*(?:{(.*?)})?\((\d+),(\d+)\)/o;

my %accents = ( "e'" => ["e'",'�'],

             'a`' => ['a`','�'],
             'e`' => ['e`', '�'],
             'u`' => ['u`','�'],

             'a^' => ['a\^','�'],
             'e^' => ['e\^','�'],
             'i^' => ['i\^','�'],
             'o^' => ['o\^','�'],
             'u^' => ['u\^','�'],

             'e"' => ['e"','�'],
             'i"' => ['i"','�'],
             'u"' => ['u"','�'],
             'y"' => ['y"','�'],

             'c,' => ['c,(?=[aou])','�']

             );

my $accentpat = join('|',map( $accents{$_}->[0], keys(%accents)));
my $accentpat = qr/($accentpat)/o;

my %formats = ( 'raw' => 'raw',
		'grammar' => 'html',
		'tree' => 'dot',
		'dependency' => 'dependency',
		'xml' => 'xml'
		);

my %gif = (
	   'tree' => 1,
	   'dependency' => 1
	   );

my %graphics = ( 'gif' => { type => 'image/gif', 
			    dot => '-Gbgcolor=linen -Gcolor=palegreen -Gstyle=filled', 
#			    dot => '-Gbgcolor=grey',
			    attachement => 'parser.gif' },
		 'png' => { type => 'image/png', 
			    dot => '-Gbgcolor=linen', 
			    attachement => 'parser.png' },
		 'ps' => {  type => 'application/postscript',
			    dot => '-Gsize="8,8" -Gmargin=".5,4"',
			    attachement => 'parser.ps'
			    },
		 'dot' => { type=> 'text/plain',
			    attachement => 'parser.dot'
			    },
		 'xml' => { type=> 'text/xml',
			    attachement => 'parser.xml'
			    },
		 'svgz' => { type=> 'image/svg+xml',
			    attachement => 'parser.svgz'
			    }
		 );

######################################################################
# Configuration 

delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

my $server = { 'host' => 'localhost',
	       'port' => '8999',
	   };

my %grammars = ();
my %labels = ();

if (!%grammars) {
    my $handle = send_server($server,"help\nquit");

    while (<$handle> !~ /^Parsers:/) {
	next;
    }
    while (<$handle> =~ /^\s*(\w+)\s+--\s*(.*)/) {
	$grammars{$1} = { 'server' => $server, 'label' => "$2" };
	$labels{$1} = "$2";
    }
    close($handle);
}

my %options = ( 'forest' => [ '-forest' ],
		'trace' => ['-v','dyam']
		);

######################################################################
# Reading params

my $path_info = $query->path_info;

my $file = $query->upload('filename');

if (!$file && $query->cgi_error) {
    print $query->header(-status=>$query->cgi_error);
    exit 0;
}

if ($file) {
    my $type = $query->uploadInfo($file)->{'Content-Type'};
    unless ($type eq 'text/plain') {
	die "PLAIN FILES ONLY!";
    }
}

my ($grammar) = $query->param(-name=>'grammar');

my ($examples) = $query->param(-name=>'examples');

my ($format) = $query->param(-name=>'forest');

my @history=();

my ($sentence) = &clean_sentence($query->param(-name=>'sentence'));

my $turned_on = $query->param('save_history');

if (defined($file) && "$file") {
    @history=();
    while (<$file>){
	next if /^\d*\s*$/;
	$_ =  &clean_sentence($_);
	push(@history,$_) unless (/^$/);
    }
} elsif ("$examples" eq 'on') {
    @history = ();
    my $handle = send_server($grammars{$grammar}{'server'},<<MSG);
examples $grammar
quit
MSG
    while (<$handle>) {
	push(@history,&clean_sentence($_));
    }
    close($handle);
    $query->param('examples', 'off');
} elsif ("$turned_on" eq 'on') {
    @history = $query->cookie('dyalog');
} else {
    @history = $query->param('hidden');
}

if ($sentence && ! grep("$_" eq $sentence,@history)) {
    @history=() if ($history[0] eq '<none>');
    @history=($sentence,@history);
#    $query->param(-name=>'history',-values=>[$sentence]);
}

if (!@history) {
    @history=('<none>');
}

$query->param(-name=>'history',-values=>[@history]);
$query->param('hidden',@history);

#my @options = $query->param('options');

######################################################################
# Emitting form

if ($path_info =~ m%/(gif|png|ps|dot|svgz)%) {
    my $type = $1;
    print $query->header(-type=> $graphics{$type}{type},
			 -attachement=> $graphics{$type}{attachement},
			 -expires=>'+1h');
    goto RESULT;
} elsif ($format eq 'xml') {
    my $type = 'xml';
    print $query->header(-type=> $graphics{$type}{type},
			 -attachement=> $graphics{$type}{attachement},
			 -expires=>'+1h');
    goto RESULT;
}

my $cookie;

if (defined $turned_on && ("$turned_on" eq 'on')) {
    $cookie = $query->cookie(-name=>'dyalog',
			     -value=>\@history,
			     -expires=>'+1h',
			     );
    print $query->header(-cookie=>$cookie);
} else {
    print $query->header;
}

my $JSCRIPT=<<END;
// Put selected history line into sentence part (erasing old sentence)
    function paste_into_sentence(element) {
	var sentence = element.options[element.selectedIndex].text;
	document.form1.sentence.value = sentence;
	document.form1.sentence.focus();
	return true;
    }
END

my $STYLE=<<END;
<!--
    BODY {background-color: linen;}
-->
END

print $query->start_html( -title => 'Parsing on line',
			  -script=> $JSCRIPT,
			  -style => { -code => $STYLE }
			  );
print h1('Parsing on line');
print $query->start_multipart_form(-name=>'form1');
print p('Select a parser ',
	br,
	$query->popup_menu(-name=>'grammar',
			   -values=> [ sort {$labels{$a} cmp $labels{$b}} keys %labels ],
			   -labels => \%labels,
			   -default=>'xl'
			   ),
	$query->checkbox(-name=>'examples',
			 -value=>'on',
			 -label=>'Load default examples'),
	a({href=>'http://alpage.inria.fr/~clerger/help.html'},"Help")		
	);
print p('Forest format: ',
	$query->popup_menu( -name=>'forest',
			    -values=> [ sort keys %formats ],
			    -default=> 'dependency')
	);
print p('Enter a sentence or select one in the history popup menu',
	br,
	$query->textarea(-name=>'sentence', 
			 -rows=>3, -columns=>70));

print p('History popup menu',
	br,
	$query->popup_menu(-name=>'history',
			   -onChange=>'paste_into_sentence(this)'
			   ),
	$query->hidden(-name=>'hidden',-default=>[@history]),
	$query->checkbox(-name=>'save_history',
			 -value=>'on',
			 -label=>'Save/Restore history (using a cookie)')
	);

print p('Load your own file as history popup menu (a sentence per line)',
	br,
	$query->filefield(-name=>'filename',
			  -size=>50
			  ));

print p($query->reset,$query->submit('Action','Submit'));

print $query->endform;
print "<HR>\n";

######################################################################
# Emitting result

 RESULT:

goto NOTHING unless ($grammar && $sentence);

#print "path_info=$path_info";

if ($gif{$format} && !($path_info =~ m%/(gif|png|ps|dot|svgz)%)) {
    # delegate the work to a sub query !
    my $url = $query->self_url;
    $url =~ s/(history|hidden)=(.*?);//g;
    my $psurl = $url;
    $psurl =~ s%(parser\.cgi)%$1/ps%;
    my $pngurl = $url;
    $pngurl =~ s%(parser\.cgi)%$1/png%;
    my $doturl = $url;
    $doturl =~ s%(parser\.cgi)%$1/dot%;
    my $svgurl = $url;
    $svgurl =~ s%(parser\.cgi)%$1/svgz%;
    print "[",$query->a({href=>$psurl},"PostScript"),
      "|",$query->a({href=>$pngurl},"PNG"),
      "|",$query->a({href=>$doturl},"Dot"),
	"|",$query->a({href=>$svgurl},"SVG"),"]\n";
    $url =~ s%(parser\.cgi)%$1/gif%;
    print $query->center($query->img({src=>"$url",align=>"CENTER"}));
    goto NOTHING;
}

$sentence =~ s/$accentpat/$accents{$1}->[1]/og;

open(LOG,">> /tmp/cgi_dyalog.log") || 
    die "Can't open log file";

print LOG strftime("date= %b %e %Y %H:%M:%S\n", localtime);
print LOG "host=",remote_host(),"\n";
print LOG "grammar=",$grammar,"\n";
print LOG "sentence=",$sentence,"\n";
print LOG "format=",$format,"\n";
print LOG "=\n";

close(LOG);

my $handle = send_server($grammars{$grammar}{'server'},<<MSG);
set forest $formats{$format}
$grammar $sentence
quit
MSG

#print $query->pre("FOREST FORMAT $format $formats{$format}");

if ($format eq 'raw') {
    print $query->pre(<$handle>);
} elsif ($path_info =~ m%/(gif|png|ps|dot|svgz)%) { # dot format
    my $type = $1;
    my $pid=open2(*TATA,*TOTO,"dot -T$type $graphics{$type}{dot}");
#    my $pid=open2(*TATA,*TOTO,'cat');
    print TOTO <$handle>;
    close TOTO || die "can't close: $!";
    print <TATA>;
    close TATA || die "can't close: $!";
    waitpid $pid, 0;
} else {
    print <$handle>;
}

close $handle || die "cannot close: $!";

exit if ($path_info =~ m%/(gif|png|ps|dot|svgz)% || $format eq 'xml');

 NOTHING:
    
print hr,
    address(a({href=>'mailto:Eric.Clergerie@inria.fr'},'Eric de la Clergerie')),
    a({href=>'http://pauillac.inria.fr/~clerger/'},'Home Page');

print $query->end_html;

######################################################################
# sub routines

sub clean_sentence {
    my ($sentence)=@_;
    $sentence =~ s/\s+/ /og;
    $sentence =~ s/^\s+//og;
    $sentence =~ s/\s+$//og;
    return $sentence;
}

sub send_server {
    my $server = shift;
    my $message = shift;

    my $host = $server->{'host'};
    my $port = $server->{'port'};

    my ($kidpid, $handle, $line);

    # create a tcp connection to the specified host and port
    $handle = IO::Socket::INET->new(Proto     => "tcp",
				    PeerAddr  => $host,
				    PeerPort  => $port
				)
	or die "can't connect to the server of parsers [$host:$port]";

    $handle->autoflush(1);

    # split the program into two processes, identical twins
    die "can't fork: $!" unless defined($kidpid = fork());

    # Child part
    if (!$kidpid) { # Child
	print $handle $message;
	exit 0;
    }
    
    return $handle;
}

__END__

=head1 CGI Perl Script to use DyALog on line

This script has been designed to test DyALog on line on a set of
various grammars.




    print $query->center($query->iframe({src=>"$svgurl",
					 width=>"100%",
					 height=>"200",
					 align=>"CENTER"
					},
					$query->img({src=>"$url",align=>"CENTER"})
				       ));

$query->embed({src=>"$svgurl",
					type=>$graphics{svgz}{type},
					coding=>"gzip",
					palette=>"foreground"
				       })
    print $query->object({data=>"$svgurl",
			  type=>$graphics{svgz}{type},
			  width=>"100%",
			  height=>"200"
			 },
			 $query->img({src=>"$url",align=>"CENTER"})
			);

    print $query->embed({src=>"$svgurl",
			 type=>$graphics{svgz}{type},
			 coding=>"gzip",
			 palette=>"foreground"
			});
