<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:exslt="http://exslt.org/common"
  >

<xsl:output
  method="html"
  indent="yes"
  encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:variable name="colors">
  <colors>
    <color id="GN" value="#E15573"/>
    <color id="NV" value="#7CB26D"/>
    <color id="GP" value="#9ABDF9"/>
    <color id="GR" value="#CC6600"/>
    <color id="PV" value="#999999"/>
    <color id="GA" value="#B679BF"/>
  </colors>
</xsl:variable>

<xsl:variable name="relations">
  <relations>
    <relation type="SUJ-V" title="Sujet - Verbe" color="#ffff99">
      <arg name="sujet"/>
      <arg name="verbe"/>
    </relation>
    <relation type="AUX-V" title="Auxiliaire - Verbe" color="#88dd88">
      <arg name="auxiliaire"/>
      <arg name="verbe"/>
    </relation>
    <relation type="COD-V" title="COD - Verbe" color="#ffff99">
      <arg name="COD"/>
      <arg name="verbe"/>
    </relation>
    <relation type="CPL-V" title="Compl�ment - Verbe"  color="#88dd88">
      <arg name="compl�ment"/>
      <arg name="verbe"/>
    </relation>
    <relation type="MOD-V" title="Modifieur - Verbe" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="verbe"/>
    </relation>
    <relation type="COMP" title="Compl�menteur"  color="#88dd88">
      <arg name="compl�menteur"/>
      <arg name="NV de la proposition subordonn�e"/>
    </relation>
    <relation type="ATB-SO" title="Attribut - Sujet / Objet" color="#ffff99">
      <arg name="attribut"/>
      <arg name="verbe"/>
      <arg name="sujet / objet"/>
    </relation>
    <relation type="MOD-N" title="Modifieur - Nom"  color="#88dd88">
      <arg name="modifieur"/>
      <arg name="nom"/>
      <arg name="� propager"/>
    </relation>
    <relation type="MOD-A" title="Modifieur - Adjectif" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="adjectif"/>
    </relation>
    <relation type="MOD-R" title="Modifieur - Adverbe"  color="#88dd88">
      <arg name="modifieur"/>
      <arg name="adverbe"/>
    </relation>
    <relation type="MOD-P" title="Modifieur - Pr�position" color="#ffff99">
      <arg name="modifieur"/>
      <arg name="pr�position"/>
    </relation>
    <relation type="COORD" title="Coordination"  color="#88dd88">
      <arg name="coordonnant"/>
      <arg name="coordonn� gauche"/>
      <arg name="coordonn� droit"/>
    </relation>
    <relation type="APPOS" title="Apposition" color="#ffff99">
      <arg name="premier �l�ment"/>
      <arg name="deuxi�me �l�ment"/>
    </relation>
    <relation type="JUXT" title="Juxtaposition"  color="#88dd88">
      <arg name="premier �l�ment"/>
      <arg name="deuxi�me �l�ment"/>
    </relation>
  </relations>
</xsl:variable>

<xsl:template match="Document">
  <html>
    <head>
<!--      <link rel="STYLESHEET" type="text/css" href="file:///home/vagabond/clerger/Grammars/frmg/easy.css"/> -->
      <title> HTML Passage Format </title>
    </head>
    <body bgcolor="linen">
      
      <h1> Annotations Passage</h1>

      Plus d'informations <a href="http://atoll.inria.fr/passage">ici</a>

      <xsl:apply-templates/>

    </body>
  </html>
</xsl:template>

<xsl:template match="Sentence">

  <xsl:variable name="strip" select="1+string-length(@id)"/>

  <hr/>
      
  <xsl:if test="@mode='robust'">
    <p>Partial analysis</p>
  </xsl:if>

      <h2> Constituants </h2>
      <table class="constituants" border="1">
	<!-- Enonc� -->
	<tr>
	  <td colspan="{count(//W)}" bgcolor="#99CCCC">
	    <font size="+1">
	      Enonc� <xsl:value-of select="substring(@id,2)"/>
	    </font>
	  </td>
	</tr>
        <!-- Groups -->
        <tr class="groups">
          <xsl:apply-templates select="./*" mode="groupe">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
        </tr>
        <!-- Words -->
        <tr class="words" bgcolor="lightBlue">
          <xsl:apply-templates select="//W" mode="word">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
        </tr>
        <!-- F -->
        <tr class="Fs" bgcolor="#FFFFFF">
          <xsl:apply-templates select="//W" mode="F">
	    <xsl:with-param name="strip" select="$strip"/>
	  </xsl:apply-templates>
        </tr>
      </table>

      <h2> Relations </h2>
      
      <table>

	<tr valign="top">
	  <xsl:variable name="current" select="."/>
	  <xsl:for-each select="exslt:node-set($relations)/relations/relation">
	    <xsl:variable name="type" select="@type"/>
	    <xsl:if test="$current/R[@type=$type]">
	      <td align="center">
		<h3><xsl:value-of select="position()"/>. <xsl:value-of select="@title"/></h3>
		
		<table border="1" bgcolor="{@color}">
		  <tbody>
		    <tr class="header">
		      <xsl:for-each select="arg">
			<td><xsl:value-of select="@name"/></td>
		      </xsl:for-each>
		    </tr>
		    <xsl:apply-templates select="$current/R[@type=$type]">
		      <xsl:with-param name="strip" select="$strip"/>
		    </xsl:apply-templates>
		  </tbody>
		</table>
		
	      </td>
	    </xsl:if>
	  </xsl:for-each>

	</tr>
      </table>

</xsl:template>

<xsl:template match="G" mode="groupe">
  <xsl:param name="strip"/>
  <xsl:variable name="type" select="@type"/>
  <td colspan="{count(./W)}" class="{$type}" bgcolor="{exslt:node-set($colors)/colors/color[@id=$type]/@value}">
    <xsl:value-of select="@type"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring(@id,$strip+1)"/>
  </td>
</xsl:template>

<xsl:template match="W" mode="groupe">
  <xsl:param name="strip"/>
  <td colspan="1"/>
</xsl:template>

<xsl:template match="W" mode="word">
  <xsl:param name="strip"/>
  <td title="{@mstag}">
    <xsl:value-of select="@lemma"/>
    /
    <xsl:value-of select="@pos"/>
  </td>
</xsl:template>

<xsl:template match="W" mode="F">
  <xsl:param name="strip"/>
  <td class="F">
    <xsl:value-of select="substring(@id,$strip+1)"/>
  </td>
</xsl:template>

<xsl:template match="R">
  <xsl:param name="strip"/>
  <tr>
    <xsl:for-each select="*">
      <td> 
	<xsl:apply-templates select="." mode="relarg"> 
	  <xsl:with-param name="strip" select="$strip"/>
	</xsl:apply-templates>
      </td>
    </xsl:for-each>
  </tr>
</xsl:template>

<xsl:template match="*[@ref]" mode="relarg">
  <xsl:param name="strip"/>
  <xsl:value-of select="substring(@ref,$strip)"/>
</xsl:template>

<xsl:template match="s-o" mode="relarg">
  <xsl:param name="strip"/>
  <xsl:value-of select="@valeur"/>
</xsl:template>

<xsl:template match="�-propager[@booleen='vrai']" mode="relarg">
  <xsl:param name="strip"/>
  X
</xsl:template>

</xsl:stylesheet>
