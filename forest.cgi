#!/usr/bin/perl -T

use CGI qw/:standard *pre *table :html3 -private_tempfiles/;
use CGI::Carp qw(fatalsToBrowser);
#use strict;

$CGI::POST_MAX=1024 * 100;  # max 100K posts

use POSIX;
use IPC::Open2;
use IO::Socket;

$ENV{'PATH'} = '/bin:/usr/local/gv1.5/bin/';
$ENV{'DOTFONTPATH'} = '/dos/winnt/Fonts/';

my $query = new CGI;
my $pattern = qr/(\w+)\s*(?:{(.*?)})?\((\d+),(\d+)\)/o;

my %accents = ( 'e\'' => ['e\'','�'],

             'a`' => ['a`','�'],
             'e`' => ['e`', '�'],
             'u`' => ['u`','�'],

             'a^' => ['a\^','�'],
             'e^' => ['e\^','�'],
             'i^' => ['i\^','�'],
             'o^' => ['o\^','�'],
             'u^' => ['u\^','�'],

             'e"' => ['e"','�'],
             'i"' => ['i"','�'],
             'u"' => ['u"','�'],
             'y"' => ['y"','�'],

             'c,' => ['c,(?=[aou])','�']

             );

my $accentpat = join('|',map( $accents{$_}->[0], keys(%accents)));
my $accentpat = qr/($accentpat)/o;

my %formats = ( 'raw' => 'raw',
		'grammar' => 'html',
		'tree' => 'dot',
		'dependency' => 'dependency'
		);

my %gif = (
	   'tree' => 1,
	   'dependency' => 1
	   );

######################################################################
# Configuration 

delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

my $server = { 'host' => 'medoc',
	       'port' => '8999',
	   };

######################################################################
# Reading params

my $path_info = $query->path_info;

my ($forest) = $query->param(-name=>'forest');

my $file = $query->upload('filename');

if (!$file && $query->cgi_error) {
    print $query->header(-status=>$query->cgi_error);
    exit 0;
}

if ($file) {
    my $type = $query->uploadInfo($file)->{'Content-Type'};
    unless ($type eq 'text/plain') {
	die "PLAIN FILES ONLY!";
    }
    $forest=join('',<$file>);
    $query->param(-name=>'forest',-value=>$forest);
}

my ($grammar) = $query->param(-name=>'grammar');
my ($informat) = $query->param(-name=>'inforest');
my ($outformat) = $query->param(-name=>'outforest');

my ($sentence) = &clean_sentence($query->param(-name=>'sentence'));

$sentence = 'no sentence' unless $sentence;
$grammar = 'dummy' unless $grammar;

#my @options = $query->param('options');

######################################################################
# Emitting form

if ($path_info =~ /gif/) {
    print $query->header(-type=>'image/gif',
			 -expires=>'+1h');
    goto RESULT;
}

print $query->header;

print $query->start_html( -title => 'TAG Derivation Forest Viewer'
			  );
print h1('TAG Derivation Forest Viewer');

print p(<<EOF); 
This script may be used to view derivation forests for
TAGs following various output formats
EOF

print $query->start_multipart_form(-name=>'form1');

print p('Specify your input forest file',
	br,
	$query->filefield(-name=>'filename',
			  -size=>50
			  )
	);

print p('or type your forest',
	br,
	$query->textarea(-name=>'forest',
			 -rows=>5, 
			 -columns=>70
			 )
	);

print p('Input Forest format: ',
	$query->popup_menu( -name=>'inforest',
			    -values=> [ 'lp', 'rcg', 'xtag', 'line' ],
			    -default=> 'lp')
	);

print p('Output Forest format: ',
	$query->popup_menu( -name=>'outforest',
			    -values=> [ sort keys %formats ],
			    -default=> 'dependency')
	);

print p('Parser name (optional):',
	$query->textfield(-name=>'grammar', -size=>'15', -default=> 'dummy')
	);

print p('Enter a sentence (optional):',
	br,
	$query->textarea(-name=>'sentence', 
			 -rows=>3, 
			 -columns=>70,
			 -default=> 'no sentence'
			 ));

print p($query->reset,$query->submit('Action','Submit'));

print $query->endform;
print "<HR>\n";

######################################################################
# Emitting result

 RESULT:

goto NOTHING unless ($forest);

#print "path_info=$path_info";

if ($gif{$outformat} && !($path_info =~ /gif/)) {
    # delegate the work to a sub query !
    my $url = $query->self_url;
    $url =~ s%(forest\.cgi)%$1/gif%;
    print $query->center($query->img({src=>"$url",align=>"CENTER"}));
    goto NOTHING;
}

$sentence =~ s/$accentpat/$accents{$1}->[1]/og;

open(LOG,">> /tmp/cgi_dyalog.log") || 
    die "Can't open log file";

print LOG "host=",remote_host(),"\n";
print LOG "grammar=",$grammar,"\n";
print LOG "sentence=",$sentence,"\n";
print LOG "=\n";

close(LOG);

my $handle = send_server($server,<<MSG);
set forest $formats{$outformat}
input $informat $grammar $sentence
$forest
END INPUT FOREST
quit
MSG

#print $query->pre("FOREST FORMAT $format $formats{$format}");

if ($outformat eq 'raw') {
    print $query->pre(<$handle>);
} elsif ($path_info =~ /gif/) { # dot format
    my $pid=open2(*TATA,*TOTO,'dot -Tgif');
#    my $pid=open2(*TATA,*TOTO,'cat');
    print TOTO <$handle>;
    close TOTO || die "can't close: $!";
    print <TATA>;
    close TATA || die "can't close: $!";
    waitpid $pid, 0;
} else {
    print <$handle>;
}

close $handle || die "cannot close: $!";

exit if ($path_info =~ /gif/);

 NOTHING:
    
print hr,
    address(a({href=>'mailto:Eric.Clergerie@inria.fr'},'Eric de la Clergerie')),
    a({href=>'http://pauillac.inria.fr/~clerger/'},'Home Page');

print $query->end_html;

######################################################################
# sub routines

sub clean_sentence {
    my ($sentence)=@_;
    $sentence =~ s/\s+/ /og;
    $sentence =~ s/^\s+//og;
    $sentence =~ s/\s+$//og;
    return $sentence;
}

sub send_server {
    my $server = shift;
    my $message = shift;

    my $host = $server->{'host'};
    my $port = $server->{'port'};

    my ($kidpid, $handle, $line);

    # create a tcp connection to the specified host and port
    $handle = IO::Socket::INET->new(Proto     => "tcp",
				    PeerAddr  => $host,
				    PeerPort  => $port
				)
	or die "can't connect to the server of parsers [$host:$port]";

    $handle->autoflush(1);

    # split the program into two processes, identical twins
    die "can't fork: $!" unless defined($kidpid = fork());

    # Child part
    if (!$kidpid) { # Child
	print $handle $message;
	exit 0;
    }
    
    return $handle;
}

__END__

=head1 CGI Perl Script to use DyALog on line

This script has been designed to test DyALog on line on a set of
various grammars.




